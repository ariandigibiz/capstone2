const mongoose = require("mongoose");


const productSchema = new mongoose.Schema({
    

  
    name: {
        type: String,
        required: [true, "Product name is required"]
    },
    description: {
        type: String,
        required: [true, "Description is required"]
    },
    highlight:{
        type: String,
        required: [true, "Subtitle is required"]
    },
    price: {
        type: Number,
        required: [true, "Price is required"]
    },
    isActive: {
        type: Boolean,
        default: true
    },
    createdOn: {
        type: Date,
        default: Date.now
    },
    userOrders: [
        {
            userId: {
                type: mongoose.Schema.Types.ObjectId,
                required: [true, "UserId is required"]
            },
        }
    ]
});

const Product = mongoose.model('Product', productSchema);

module.exports = Product;
