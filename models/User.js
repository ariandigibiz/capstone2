const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({


  email: {
    type: String,
    required: [true, "Email is required"],
  },
  password: {
    type: String,
    required: [true, "Password is required"],
  },
  isAdmin: {
    type: Boolean,
    default: false,
  },
  orderedProduct: [
    {
      products: {
        productId: {
          type: mongoose.Schema.Types.ObjectId,
          required: [true, "ProductId is required"],
        },
        name: {
          type: String,
          required: [true, "Product name is required"],
        },
        quantity: {
          type: Number,
          required: true,
        },
      },
      totalAmount: {
        type: Number,
        required: true,
      },
      purchasedOn: {
        type: Date,
        default: Date.now,
      },
    },
  ],
  cart: [
    {
      products: {
        productId: {
          type: mongoose.Schema.Types.ObjectId,
          required: [true, "ProductId is required"],
        },
        name: {
          type: String,
          required: [true, "Product name is required"],
        },
        quantity: {
          type: Number,
          required: true,
        },
      },
      totalAmount: {
        type: Number,
        required: true,
      },
    },
  ]
});

const User = mongoose.model("User", userSchema);

module.exports = User;
