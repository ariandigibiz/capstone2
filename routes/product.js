const express = require("express");
const productController = require("../controllers/product");
const auth = require("../auth") 
const {verify, verifyAdmin} = auth;




const router = express.Router();


router.post("/", verify, verifyAdmin, productController.addProduct);

router.get("/all", verify, verifyAdmin, productController.getAllProducts);

router.get("/", productController.getAllActiveProducts);

router.put("/:productId", verify, verifyAdmin, productController.updateProduct);

router.put("/:productId/archive", verify, verifyAdmin, productController.archiveProduct);

router.put("/:productId/activate", verify, verifyAdmin, productController.activateProduct);

router.get("/name/:productName", productController.getProductByName);

router.get("/:productId", productController.getProduct);

module.exports = router;


