const express = require("express");
const userController = require("../controllers/user");
const auth = require("../auth");
const cartController = require("../controllers/cart");
const { verify, verifyAdmin } = auth;


const router = express.Router();

router.post("/checkEmail", (req, res) => {
    userController.checkEmailExists(req.body)
        .then(resultFromController => res.send(resultFromController))
        .catch(error => res.status(500).send({ error: "An error occurred while checking email existence." }));
});

router.post("/register",(req,res)=>{
    userController.registerUser(req.body).then(resultFromController=>res.send(resultFromController))
})

router.post("/login", userController.loginUser);

router.get("/details", verify, userController.getDetails);

router.put("/profile", verify, userController.updateProfile);

router.put('/reset-password', verify, userController.resetPassword);

router.post("/cart/add/:productId", verify, cartController.addToCart);

router.get('/viewcart', verify, cartController.viewCart);

/*router.put("/cart/update/:productId", verify, cartController.updateCartItem);*/

router.delete("/cart/remove/:productId", verify, cartController.removeCartItem);


router.post('/checkout', verify, cartController.checkout);

router.post('/order-history', verify, cartController.viewOrderHistory);


module.exports = router;
