const User = require("../models/User");
const Product = require("../models/Product");
const bcrypt = require("bcrypt");
const auth = require("../auth");


module.exports.checkEmailExists = (reqbody) => {
    return User.find({ email: reqbody.email })
        .then(result => {
            if (result.length > 0) {
                return {result: true, message: "Email exists"}; 
            } else {
                return {result: false, message: "Email does not exist"}; 
            }
        });
};



module.exports.registerUser = async (reqbody) => {
    const emailRegex = /^[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/;

    if (!emailRegex.test(reqbody.email)) {
        return { success: false, message: 'Invalid email format' };
    }

    // Check if the email already exists in the database
    const existingUser = await User.findOne({ email: reqbody.email });
    if (existingUser) {
        return { success: false, message: 'Email already exists' };
    }

    const newUser = new User({
        email: reqbody.email,
        password: bcrypt.hashSync(reqbody.password, 10)
    });

    try {
        const user = await newUser.save();
        return { success: true, message: "Registration successful" };
    } catch (error) {
        console.error("Error while registering user:", error);
        return { success: false, message: "An error occurred while registering the user." };
    }
};



module.exports.loginUser = (req, res) => {
    User.findOne({ email: req.body.email })
        .then(result => {
            if (result === null) {
                return res.send(false); 
            } else {
                const isPasswordCorrect = bcrypt.compareSync(req.body.password, result.password);
                if (isPasswordCorrect) {
                    const accessToken = auth.createAccessToken(result);
                    return res.send({ access: accessToken }); 
                } else {
                    return res.send(false); 
                }
            }
        })
        .catch(err => res.send(err));
};



module.exports.getDetails = (req, res) => {

        return User.findById(req.user.id)
        .then(result => {
            result.password = "";
            return res.send(result);
        })
        .catch(err => res.send(err))
    };



// Function to update a user's profile, including email
module.exports.updateProfile = async (req, res) => {
  try {
    // Assuming verify middleware sets req.user with the user's ID
    const userId = req.user.id;

    const { newEmail, newPassword } = req.body; // Assuming new email and password are sent in the request body

    // Find the user by their ID
    const user = await User.findById(userId);

    if (!user) {
      return res.status(404).json({ success: false, message: 'User not found.' });
    }

    // Update the user's email and password if provided
    if (newEmail) {
      user.email = newEmail;
    }
    if (newPassword) {
      // Hash the new password before updating it
      const hashedPassword = await bcrypt.hash(newPassword, 10); // You can adjust the number of rounds (10 is a good default)
      user.password = hashedPassword;
    }

    // Save the updated user
    await user.save();

    res.status(200).json({ success: true, message: 'Profile updated successfully.' });
  } catch (error) {
    console.error('Error updating user profile:', error);
    res.status(500).json({ success: false, error: 'An error occurred while updating the profile.' });
  }
};


module.exports.resetPassword = async (req, res) => {
        try {

        //console.log(req.user)
        //console.log(req.body)

          const { newPassword } = req.body;
          const { id } = req.user; // Extracting user ID from the authorization header
      
          // Hashing the new password
          const hashedPassword = await bcrypt.hash(newPassword, 10);
      
          // Updating the user's password in the database
          await User.findByIdAndUpdate(id, { password: hashedPassword });
      
          // Sending a success response
          res.status(200).send({ message: 'Password reset successfully' });
        } catch (error) {
          console.error(error);
          res.status(500).send({ message: 'Internal server error' });
        }
    };




/*module.exports.checkoutCart = async (req, res) => {
    try {
        const userId = req.params.id; // Use req.params.id to access the id

        // Find the user by id
        const user = await User.findById(userId);
        if (!user) {
            return res.status(404).send({ success: false, message: "User not found." });
        }

        const orderedProducts = user.orderedProduct;

        let totalAmount = 0;
        for (const orderedProduct of orderedProducts) {
            totalAmount += orderedProduct.totalAmount;
        }

        const cartSummary = orderedProducts.map(orderedProduct => ({
            productId: orderedProduct.products.productId,
            quantity: orderedProduct.products.quantity,
        }));

        console.log("Ordered Products:", orderedProducts);

        return res.status(200).send({
            success: true,
            orderedProduct: cartSummary,
            totalAmount: totalAmount,
        });

    } catch (error) {
        console.error("Error in checkout cart controller:", error);
        return res.status(500).send({ success: false, error: "An error occurred while processing the cart." });
    }
};
*/





/*module.exports.updateCartItemQuantity = async (req, res) => {
    try {
       
        const cartItem = user.cart.find(item => item.productId.toString() === productId);
        if (!cartItem) {
            return res.status(404).send({ success: false, error: "Product not found in cart." });
        }
        cartItem.quantity = quantity;
        cartItem.subtotal = userController.calculateSubtotal(cartItem.product, quantity);

       
        await user.save();

        return res.send({ success: true, message: "Cart item quantity updated successfully." });
    } catch (error) {
       
    }
};
*/

/*module.exports.addToCart = async (req, res) => {
    try {
        const productId = req.params._id;
        const quantity = req.body.quantity;

      
        console.log("productId");
        const product = await Product.findById(productId);

        console.log(productId);

        if (!product || !product.isActive) {
            return res.status(404).send({ success: false, message: "Product not found." });
        }

       
        const subtotal = product.price * quantity;

        
        const updatedUser = await User.findByIdAndUpdate(
            req.user.id,
            {
                $push: {
                    cart: {
                        productId: productId,
                        quantity: quantity,
                        subtotal: subtotal
                    }
                }
            },
            { new: true }
        );

        return res.send({ success: true, user: updatedUser });
    } catch (error) {
        return res.status(500).send({ success: false, error: "An error occurred while adding to cart." });
    }
};
*/


/*module.exports.removeCartItem = async (req, res) => {
    try {
        const productId = req.params.productId;

      
        const user = await User.findById(req.user.id);

        if (!user) {
            return res.status(404).send({ success: false, message: "User not found." });
        }

        
        const updatedCart = user.cart.filter(cartItem => cartItem.productId.toString() !== productId);

      
        user.cart = updatedCart;

       
        await user.save();

        return res.send({ success: true, message: "Item removed from the cart.", user: user });
    } catch (error) {
        return res.status(500).send({ success: false, error: "An error occurred while removing the item from the cart." });
    }
};


*/



/*module.exports.viewCart = async (req, res) => {
  try {
    // Assuming you have a user ID in your request, replace 'userId' with the actual field name
    const userId = req.user.id; // Replace 'id' with the actual field name in your user object

    // Find the user by their ID
    const user = await User.findById(userId);
    console.log(userId);

    if (!user) {
      return res.status(404).json({ message: 'User not found' });
    }

    // Access the 'orderedProduct' array in the user object
    const orderedProducts = user.orderedProduct;

    // Calculate the total quantity for each product
    const productQuantities = {};

    orderedProducts.forEach((order) => {
      const productId = order.products.productId;
      const quantity = order.products.quantity;

      if (productQuantities[productId]) {
        productQuantities[productId] += quantity;
      } else {
        productQuantities[productId] = quantity;
      }
    });

    // Now, 'productQuantities' contains the total quantity for each product

    return res.status(200).json({ orderedProducts, productQuantities });
  } catch (error) {
    console.error(error);
    return res.status(500).json({ message: 'Internal server error' });
  }
};
*/