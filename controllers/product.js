const Product = require("../models/Product");


module.exports.addProduct = async (req, res) => {
    try {
        const loggedInUserId = req.user.id; // Rename the variable
        const { name, description, highlight, price } = req.body; // Remove userId from destructuring

        // Create a new product instance
        const newProduct = new Product({
            name,
            description,
            highlight,
            price,
            userOrders: [{ userId: loggedInUserId }] // Use the renamed variable
        });

        // Save the new product to the database
        await newProduct.save();

        console.log(newProduct);
        return res.send({ success: true, message: "Product added successfully" });
    } catch (error) {
        if (error.code === 11000) {
            // Handle duplicate key error
            return res.status(400).send({ success: false, message: "Product with this ID already exists" });
        }
        console.error("Error while adding product:", error);
        return res.status(500).send({ success: false, message: "An error occurred while adding the product" });
    }
};



module.exports.getAllProducts = (req, res) => {
    if (!req.user.isAdmin) {
        return res.status(403).send({ success: false, message: "Permission denied. Admin access required." });
    }

    return Product.find({}, '_id name description price quantity isActive') // Select the required fields
        .then(result => {
            return res.send(result);
        })
        .catch(err => res.status(500).send({ success: false, error: "An error occurred while fetching products." }));
};


module.exports.getAllActiveProducts = (req, res) => {
    return Product.find({ isActive: true })
        .then(result => {
            return res.send(result);
        });
};


module.exports.updateProduct = (req, res) => {
  
 const { name, description, highlight, price } = req.body;

  Product.findOneAndUpdate(
    { _id: req.params.productId },
    { $set: { name, description, highlight, price } },
    { new: true } 
  )
    .then((product) => {
      if (!product) {
        return res.status(404).send({ success: false, message: "Product not found." });
      }
      return res.send({ success: true, product });
    })
    .catch((error) => {
      return res.status(500).send({ success: false, error: "An error occurred while updating the product." });
    });
};


module.exports.archiveProduct = (req, res) => {
    

    return Product.findByIdAndUpdate(req.params.productId, { isActive: false })
        .then(product => {
            return res.send({ success: true, product: product });
        })
        .catch(error => {
            return res.status(500).send({ success: false, error: "An error occurred while archiving the product." });
        });
};

module.exports.activateProduct = (req, res) => {
    

    return Product.findByIdAndUpdate(req.params.productId, { isActive: true })
        .then(product => {
            return res.send({ success: true, product: product });
        })
        .catch(error => {
            return res.status(500).send({ success: false, error: "An error occurred while activating the product." });
        });
};


module.exports.getProductByName = (req, res) => {
  const productName = req.params.productName;

  
  return Product.findOne({ name: productName })
    .then((result) => {
      if (!result) {
        return res.status(404).json({ error: 'Product not found' });
      }
      return res.json(result);
    })
    .catch((error) => {
      console.error('Error retrieving product by name:', error);
      return res.status(500).json({ error: 'An error occurred while retrieving the product by name.' });
    });
};


module.exports.getProduct = (req, res) => {
  Product.findById(req.params.productId)
    .populate({
      path: 'userOrders.userId', // Populate the userId field in userOrders
      select: 'email', // Select the fields you want to include from the User model
    })
    .then(result => {
      return res.send(result);
    })
    .catch(error => {
      return res.status(500).json({ error: 'Error fetching product', message: error.message });
    });
};


/*module.exports.addProduct = (req, res) => {
    let newProduct = new Product({
        name: req.body.name,
        description: req.body.description,
        highlight: req.body.highlight,
        price: req.body.price
    });

    return newProduct.save()
        .then((product) => {
            console.log(newProduct);
            return res.send({ success: true, message: "Product added successfully" });
        })
        .catch((error) => {
            if (error.code === 11000) {
                // Handle duplicate key error
                return res.status(400).send({ success: false, message: "Product with this ID already exists" });
            }
            console.error("Error while adding product:", error);
            return res.status(500).send({ success: false, message: "An error occurred while adding the product" });
        });
};*/
