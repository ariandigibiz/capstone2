const User = require("../models/User");
const Product = require("../models/Product");
const bcrypt = require("bcrypt");
const auth = require("../auth");




module.exports.addToCart = async (req, res) => {
    try {
        const productId = req.params.productId;
        const quantity = parseInt(req.body.quantity); // Parse the quantity as an integer

        if (isNaN(quantity) || quantity <= 0) {
            return res.status(400).send({ success: false, message: "Invalid quantity." });
        }

        const product = await Product.findById(productId);

        if (!product || !product.isActive) {
            return res.status(404).send({ success: false, message: "Product not found." });
        }

        // Find the user
        const user = await User.findById(req.user.id);

        if (!user) {
            return res.status(404).send({ success: false, message: "User not found." });
        }

        // Check if the product is already in the cart
        const existingCartItemIndex = user.cart.findIndex(item => item.products.productId.toString() === productId);

        if (existingCartItemIndex !== -1) {
            // If the product exists in the cart, update the quantity
            user.cart[existingCartItemIndex].products.quantity += quantity;
            user.cart[existingCartItemIndex].totalAmount += product.price * quantity;
        } else {
            // If the product is not in the cart, add it as a new item
            const totalAmount = product.price * quantity;
            user.cart.push({
                products: {
                    productId: productId,
                    name: product.name,
                    quantity: quantity
                },
                totalAmount: totalAmount
            });
        }

        // Save the user with the updated cart
        await user.save();

        // Calculate the totalPrice by summing up all totalAmount values in the user's cart
        const totalPrice = user.cart.reduce((total, order) => total + order.totalAmount, 0);

        // Only include the cart data in the response, not the orderedProduct data
        const responseData = {
            success: true,
            user: {
                _id: user._id,
                /*email: user.email,
                isAdmin: user.isAdmin,*/
                cart: user.cart,
            },
            totalPrice,
        };

        return res.send(responseData);
    } catch (error) {
        console.error("Error adding to cart:", error);
        return res.status(500).send({ success: false, error: "An error occurred while adding to cart." });
    }
};




module.exports.viewCart = async (req, res) => {
    try {
        const userId = req.user.id; // Get the user's ID from the authenticated request

        // Find the user's document and populate the cart.products.productId field
        const user = await User.findById(userId).populate('cart.products.productId');

        if (!user) {
            return res.status(404).json({ message: 'User not found' });
        }

        // Extract cart items from the user's cart
        const cartItems = user.cart.map((order) => {
            const product = order.products;
            return {
                productId: product.productId._id,
                name: product.name,
                price: product.price,
                quantity: product.quantity,
                totalAmount: order.totalAmount, // Include totalAmount for each product
            };
        });

        // Now, you have an array of cart items with additional data

        res.status(200).json({ cartItems });
    } catch (error) {
        console.error(error);
        res.status(500).json({ message: 'Error fetching cart items' });
    }
};

module.exports.checkout = async (req, res) => {
    try {
        const userId = req.user.id;

        // Find the user by their ID
        const user = await User.findById(userId);

        if (!user) {
            return res.status(404).send({ success: false, message: "User not found." });
        }

        // Get the items from the user's cart
        const cartItems = user.cart;

        if (cartItems.length === 0) {
            return res.status(400).send({ success: false, message: "Cart is empty." });
        }

        // Move the items from the cart to the orderedProduct array
        user.orderedProduct.push(...cartItems);

        // Clear the user's cart by setting it to an empty array
        user.cart = [];

        // Save the user with the updated cart and orderedProduct
        await user.save();

        // Update the userorders array in each product
        for (const cartItem of cartItems) {
            const productId = cartItem.products.productId;

            // Find the product and update its userOrders array
            await Product.findByIdAndUpdate(
                productId,
                { $addToSet: { userOrders: { userId: userId } } }, // Add userId to the userOrders array, avoiding duplicates
                { new: true }
            );
        }

        // Return a simplified response indicating a successful checkout
        return res.send({ success: true, message: "Checkout successful." });
    } catch (error) {
        console.error("Error during checkout:", error);
        return res.status(500).send({ success: false, error: "An error occurred during checkout." });
    }
};









module.exports.removeCartItem = async (req, res) => {
    try {
        const userId = req.user.id;
        const productId = req.params.productId;

        // Find the user by their ID
        const user = await User.findById(userId);
        console.log(userId);

        if (!user) {
            return res.status(404).send({ success: false, message: "User not found." });
        }

        // Find the index of the item to remove in the user's cart
        const cartItemIndex = user.cart.findIndex(
            (item) => item.products.productId.toString() === productId
        );

        if (cartItemIndex === -1) {
            return res.status(404).send({ success: false, message: "Item not found in the cart." });
        }

        // Remove the item from the cart array
        user.cart.splice(cartItemIndex, 1);

        // Save the user with the updated cart
        await user.save();

        return res.send({ success: true, message: "Item removed from the cart." });
    } catch (error) {
        console.error("Error removing item from cart:", error);
        return res
            .status(500)
            .send({ success: false, error: "An error occurred while removing item from the cart." });
    }
};





module.exports.viewOrderHistory = async (req, res) => {
    try {
        const userId = req.user.id;

        // Find the user by their ID
        const user = await User.findById(userId);

        if (!user) {
            return res.status(404).send({ success: false, message: "User not found." });
        }

        // Get the ordered products from the historical order data
        const orderHistory = user.orderedProduct;

        // Return the order history data (ordered product data)
        return res.send({ success: true, orderHistory });
    } catch (error) {
        return res.status(500).send({ success: false, error: "An error occurred while retrieving order history." });
    }
};







/*module.exports.updateCartItem = async (req, res) => {
    try {
        const productId = req.params.productId;
        const newQuantity = req.body.quantity;

        const updatedUser = await User.findOneAndUpdate(
            { _id: req.user.id, "orderedProduct.products.productId": productId },
            { $set: { "orderedProduct.$.products.quantity": newQuantity } },
            { new: true }
        );

        return res.send({ success: true, user: updatedUser });
    } catch (error) {
        return res.status(500).send({ success: false, error: "An error occurred while updating the cart item." });
    }
};
*/



/*module.exports.removeCartItem = async (req, res) => {
  try {
    const productId = req.params.productId;
    const quantityToRemove = req.body.quantity;

    const user = await User.findById(req.user.id);

    if (!user) {
      return res.status(404).send({ success: false, message: "User not found." });
    }

    const cartItem = user.orderedProduct.find(
      (cartItem) => cartItem.products.productId.toString() === productId
    );

    if (!cartItem) {
      return res.status(404).send({ success: false, message: "Item not found in the cart." });
    }

    const currentQuantity = cartItem.products.quantity;

    if (quantityToRemove <= currentQuantity) {
      // Deduct the quantityToRemove from the current quantity
      cartItem.products.quantity -= quantityToRemove;

      await user.save();

      return res.send({
        success: true,
        message: "Item removed from the cart.",
        productId: productId,
        newQuantity: cartItem.products.quantity,
      });
    } else {
      return res.status(400).send({
        success: false,
        message: "Quantity to remove exceeds the current quantity in the cart.",
        productId: productId,
        currentQuantity: currentQuantity,
      });
    }
  } catch (error) {
    console.error("Error:", error);
    return res.status(500).send({
      success: false,
      error: "An error occurred while removing the item from the cart.",
    });
  }
};
*/















