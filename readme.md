## E-COMMERCE API DOCUMENTATION

***TEST ACCOUNTS:***
- Regular User:
     - email: adminyan@mail.com
     - pwd: adminyan
- Admin User:
    - email: custbarbie@mail.com
    - pwd: custbarbie
    

***ROUTES:***
- User registration (POST)
	- http://localhost:4000/users/register
    - request body: 
        - email (string)
        - password (string)
- User authentication (POST)
	- http://localhost:4000/users/login
    - request body: 
        - email (string)
        - password (string)
- Create Product (Admin only) (POST)
	- http://localhost:4000/products
    - request body: 
        - name (string)
        - description (string)
        - price (number)
- Retrieve all products (Admin only) (GET)
	- http://localhost:4000/products/all
    - request body: none